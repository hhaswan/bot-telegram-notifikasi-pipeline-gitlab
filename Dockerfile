# Gunakan image Python resmi sebagai dasar
FROM python:3.9-slim

# Tetapkan direktori kerja di dalam container
WORKDIR /app

# Salin file requirements.txt ke dalam container
COPY requirements.txt .

# Install library yang diperlukan menggunakan pip
RUN pip install --no-cache-dir -r requirements.txt

# Salin semua file sumber kode Python ke dalam container
COPY . .

# Beri tahu Docker bahwa container akan mendengarkan pada port 5000
EXPOSE 5000

# Perintah untuk menjalankan script Python ketika container di-start
CMD ["python", "./bot-gitlab.py"]