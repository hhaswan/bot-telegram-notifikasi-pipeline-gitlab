from telegram import Update
from telegram.ext import Updater, CommandHandler, CallbackContext, JobQueue
import requests
import os
import pytz
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger


TOKEN = os.getenv('TELEGRAM_TOKEN')
GITLAB_TOKEN = os.getenv('GITLAB_TOKEN')

scheduler = BackgroundScheduler(timezone=pytz.timezone('Asia/Makassar'))


def start(update: Update, context: CallbackContext):
    update.message.reply_text('Selamat datang! Silakan daftar dengan perintah /register [project_id] untuk mendapatkan update.')

def register(update: Update, context: CallbackContext):
    user_id = update.message.chat_id
    project_id = ' '.join(context.args)
    context.bot_data[user_id] = project_id
    update.message.reply_text(f'Anda telah terdaftar untuk menerima update proyek dengan ID {project_id}.')

    # Menambahkan job ke scheduler
    scheduler.add_job(check_gitlab_project, 'interval', minutes=1, args=[context, user_id, project_id], id=str(user_id))

def check_gitlab_project(context, user_id, project_id):
    url = f"https://gitlab.com/api/v4/projects/{project_id}/pipelines"
    headers = {'PRIVATE-TOKEN': GITLAB_TOKEN}
    response = requests.get(url, headers=headers)
    pipelines = response.json()
    if pipelines:
        last_status = pipelines[0]['status']
        context.bot.send_message(chat_id=user_id, text=f"Update Terbaru Pipeline: {last_status}")

def stop(update: Update, context: CallbackContext):
    user_id = update.message.chat_id
    if scheduler.get_job(str(user_id)):
        scheduler.remove_job(str(user_id))
        update.message.reply_text('Anda telah berhenti menerima update.')
    else:
        update.message.reply_text('Anda tidak terdaftar.')

def main():
    updater = Updater(TOKEN, use_context=True)
    dp = updater.dispatcher
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("register", register, pass_args=True))
    dp.add_handler(CommandHandler("stop", stop))
    updater.start_polling()
    scheduler.start()
    updater.idle()

if __name__ == '__main__':
    main()