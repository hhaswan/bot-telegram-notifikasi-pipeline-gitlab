# Bot Telegram Notifikasi Pipeline Gitlab


## Getting started

Halo ini Bot Telegram notifikasi untuk pipeline Gitlab creted by Hasbullah Marwan

## How to

Edit file `docker-compose.yml` :

- Sesuaikan environment

```
TELEGRAM_TOKEN=your_telegram_bot_token
GITLAB_TOKEN=your_gitlab_access_token
```

- Sesuaikan port nya


Running container docker :
```
docker-compose build
docker-compose up -d
```